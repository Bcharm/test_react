'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
const gulpif = require('gulp-if');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();
const reload = browserSync.reload;


// Compile sass files to css

var SCSS_SRC = './src/Assets/scss/**/*.scss';
var  SCSS_DEST = './Assets/css';

gulp.task('complie_scss',function(){
    gulp.src(SCSS_SRC)
        .pipe(sass().on('error',saa.logError))
        .pipe(minifyCss())
        .pipe(rename({suffix:'.min'}))
        .pipe(changed(SCSS_DEST))
        .pipe(gulp.dest(SCSS_DEST))
});
//detect changs in SCSS
gulp.task('watch_scss',function(){
    gulp.watch(SCSS_SRC,[compile_scss]);
});

//run tasks
gulp.task('default',[watch_scss]);